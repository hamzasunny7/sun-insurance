const dotenv = require('dotenv').config();
if (dotenv.error) {//Just in case there is no .env file throw exception
    throw "Error: Unable to load .env file";
}
const http = require('http');
const cluster = require('cluster');
const app = require('./app');
const {bunyanApiLogName, bunyanCronApiLogName, bunyanApiLog, bunyanCronApiLog} = require('./api/utils/logger');
const numCPUs = require('os').cpus().length;
const _ = require('lodash');
const numClusters = (numCPUs * 1.4).toFixed();//e.g 24 Cores will run 33
const {createTerminus} = require('@godaddy/terminus');

const port = process.env.PORT || 3000;

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is setting up ${numClusters} workers...`);
    // Fork workers.
    for (let i = 0; i < numClusters; i++) {
        cluster.fork();
    }
    cluster.on('online', function (worker) {
        // console.log(`Worker ${worker.process.pid} is online.`);
    });
    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
        cluster.fork(); // Create a New Worker, If Worker is Dead
    });
    // Collect logger messages from child processes and output them to the
    // master's logger instance. This avoids any wackiness with multiple
    // processes trying to write to the same stream (particularly if the
    // `rotating-file` stream type is used)
    //
    // **See**: https://github.com/trentm/node-bunyan/issues/117
    _.each(cluster.workers, function (worker) {
        worker.process.on('message', function onMessage(message) {
            if (message['type'] === 'api_info' || message['type'] === 'cron_info') {
                let name = "";
                if (message['type'] === 'api_info') name = `${bunyanApiLogName}_${worker.id}`;
                if (message['type'] === 'cron_info') name = `${bunyanCronApiLogName}_${worker.id}`;
                // Prepend worker ID to message. Make sure string replacement
                // still works.
                let messageArguments = [message['message'][0]];
                if (_.isObject(messageArguments[0])) {
                    messageArguments[0].name = name;
                }
                else {
                    messageArguments.push(messageArguments[0]);
                    messageArguments[0] = {name: name};
                }

                // Concat the rest of the items in `message.message` and apply
                // it to the `logger` function based on the `level` specified.
                //
                // i.e. `message.level = "warn"`, so call `logger.warn`
                messageArguments = messageArguments.concat(message['message'].slice(1));
                if (message['type'] === 'api_info') {
                    bunyanApiLog[message['level']].apply(bunyanApiLog, messageArguments);
                }
                else if (message['type'] === 'cron_info') {
                    bunyanCronApiLog[message['level']].apply(bunyanCronApiLog, messageArguments);
                }
            }
        });
    });
}
else {
    const server = http.createServer(app);
    // intercept termination signals and attempt graceful shutdown
    createTerminus(server, {
        signals: ['SIGTERM', 'SIGINT'],
        sendFailuresDuringShutdown: true,
        onSignal,
        onShutdown,
        onSendFailureDuringShutdown
    });

    function onSendFailureDuringShutdown() {
        console.log('sending 503 error');
    }

    function onSignal() {
        console.log('Server is starting cleanup');
    }

    function onShutdown() {
        console.log('cleanup finished, server is shutting down');
    }

    server.listen(port, () => {
        if (cluster.worker.id == 1) {//Print Only 1 Time
            console.log(`Listening on port http://${server.address().address}:${server.address().port} At ${new Date().toString()} On ${process.env.NODE_ENV} Server as Worker ${cluster.worker.id} running @ process ${cluster.worker.process.pid}`);
        }
    });
}
