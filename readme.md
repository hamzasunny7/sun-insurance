
# Sun Insurance

This is a test project for Ropstam BPO interview process


## Deployment

To deploy this project first run copy .env.example to .env & give email credentials. Then run below command to install required dependencies:

```bash
  npm install
```

Then run below command to start node server:
```bash
  npm run watch
```

