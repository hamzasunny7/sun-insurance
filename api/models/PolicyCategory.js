const mongoose = require("mongoose");

const categorySchema = new mongoose.Schema({
    name: { type: String, default: null },
    type: { type: String, enum: ["health", "car", "house"] },
    created_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model("policy_category", categorySchema);