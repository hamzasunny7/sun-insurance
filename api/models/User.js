const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    name: { type: String, default: null },
    username: { type: String, unique: true },
    email: { type: String, unique: true },
    password: { type: String },
    age: { type: Number, default: null },
    income: { type: Number, default: null },
    policy_type: { type: String, enum: ["health", "car", "house"] },
    created_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model("user", userSchema);