const {isEmpty, getCurrentTimeStampWithMs} = require('../utils/utils');
const {bunyanApiLogPrint} = require('../utils/logger');
const moment = require('moment');
const now = require('performance-now');
const port = process.env.PORT || 3000;


//Use to create Log via Bunyan
function createUserApiLog(req, res) {
    let json_req = res.locals.json_req;
    if (!isEmpty(json_req)) {//Make sure when it's verified request only then log it
        let {request_id, audit_app, app_version, api_name, ip} = res.locals.audit_p_detail;//Clean Way to Define Variables From Json

        let res_body = res.locals.res_body;
        let startTime = res.locals.startTime;
        let startTimeStamp = res.locals.startTimeStamp;
        let executionTime = (now() - startTimeStamp).toFixed(3) + "ms";
        let endTime = moment().format("HH:mm:ss.SSS");
        let executionLog = {startTime, endTime, executionTime};

        let print_log = {
            time_stamp: getCurrentTimeStampWithMs(),
            request_id: request_id,
            api_name: api_name,
            port: port,
            type: "audit_api",//Although this will be override but it is for log order
            api_detail: {
                status_code: res.statusCode,
                request: json_req,
                response: res_body,
                benchmark: executionLog
            }
        };
       
        print_log['type'] = "audit_api";
        print_log['audit_p_detail'] = audit_app;
        if (app_version != "0") print_log['app_version'] = app_version;
        print_log['ip'] = ip;
        bunyanApiLogPrint(print_log);
    }
    else {
        console.log("res.locals is null");
    }
}


function auditPersonJson(req, request_id) {
    return {
        request_id: request_id,
        audit_app: isEmpty(req.headers['app-flavour']) ? "Postman" : req.headers['app-flavour'],
        app_version: isEmpty(req.headers['app-version-code']) ? "0" : req.headers['app-version-code'],
        api_name: req.originalUrl.replace(/\?.*$/, '').replace('/' + process.env.ROUTE + '/', ''),//Get only api name
        ip: req.clientIp
    }
}


module.exports = {
    createUserApiLog,
    auditPersonJson
};
