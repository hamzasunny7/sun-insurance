const mongoose = require("mongoose");

const policySchema = new mongoose.Schema({
    name: { type: String },
    type: { type: String, enum: ["health", "car", "house"] },
    min_income: { type: Number },
    per_month: { type: Number },
    mature_amount: { type: Number },
    years: { type: Number },
    created_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model("policy", policySchema);