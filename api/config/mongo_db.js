const MongoClient = require('mongodb').MongoClient;
const {isProduction} = require('../utils/utils');
const thunky = require('thunky');
const cluster = require('cluster');
let pool = require("./sqlpool.js");

const connectionOptions = {
    maxPoolSize: process.env.MONGO_POOL_SIZE || 5, //Default Pool Size is 5
    authMechanism: process.env.MONGO_DB_AUTH_MECHNAISM
};

let url = `mongodb://${process.env.MONGO_DB_HOST}:${process.env.MONGO_DB_PORT}/${process.env.MONGO_DB}`;
if (isProduction()) {
    url = `mongodb://${encodeURIComponent(process.env.MONGO_DB_USERNAME)}:${encodeURIComponent(process.env.MONGO_DB_PASSWORD)}@${process.env.MONGO_DB_HOST}:${process.env.MONGO_DB_PORT}/?${new URLSearchParams(connectionOptions).toString()}`;
}
let mongoClient;
let clusterName = cluster.isMaster ? "Master " : "Worker (" + cluster.worker.id + ") ";

//Thunky is being used to implement a lazy evaluation pattern which means only use it when called, it can also cache connection
let initializeConnection = thunky(function (callback) {
    MongoClient.connect(url, function (err, _mongoClient) {
        if (!err) {
            console.log(clusterName + " successfully connected with MongoDb")
            mongoClient = _mongoClient;
            callback(null, mongoClient.db(process.env.MONGO_DB));
        }
        else {
            callback(err, null);
        }
    });
});


//Proper shutting down connection, so running queires dont get disturbed
function gracefulShutdown() {
    if (mongoClient) {
        console.log(clusterName + "MongoDb going to close connection.");
        mongoClient.close(false, () => {
            console.log(clusterName + "MongoDb connection closed.");
            mongoClient = null;
            checkIfConnectionEnded();
        })
    }
    else {
        console.log(clusterName + "no MongoDb to close.");
        checkIfConnectionEnded();
    }

    if (pool) {
        console.log(clusterName + "mysql going to close connection.");
        shouldWeStopDb().then(function () {
            pool.end(function (err) {
                if (err) {
                    console.log(clusterName + "mysql endPool err: " + formatErrorString(err));
                }
                else {
                    console.log(clusterName + "All connections in the mysql pool have ended.");
                }
                pool = null;
                checkIfConnectionEnded();
            });
        });
    }
    else {
        console.log(clusterName + "no mysql to close.");
        checkIfConnectionEnded();
    }
}

function checkIfConnectionEnded() {
    if (pool == null && mongoClient == null) {
        console.log(clusterName + "Both connection ended")
        process.exit();
    }
}

function shouldWeStopDb() {
    return new Promise(function (resolve, reject) {
        const timer = setInterval(function () {
            // Ensure all queries has been executed before exiting the script
            const connectionQueue = pool._connectionQueue.length;
            const connectionTotal = pool._allConnections.length;
            const connectionFree = pool._freeConnections.length;//When ping user is outside of insert location
            const info = {
                connTotal: pool._allConnections.length,
                connFree: pool._freeConnections.length,
                connAcquired: pool._acquiringConnections.length,
                queryQueued: pool._connectionQueue.length
            };
            console.log(clusterName + JSON.stringify(info));
            if (connectionQueue == 0 && connectionFree == connectionTotal) {
                clearInterval(timer);
                console.log(clusterName + "All queries has been completed.");
                return resolve();
            }
        }, 10);//50
    });
}

if (cluster.isMaster) {
    // Begin reading from stdin so the process does not exit.
    process.stdin.resume();
}
// listen for the signal interruption (ctrl-c)
process.on('SIGINT', gracefulShutdown);
process.on('SIGTERM', gracefulShutdown);


module.exports = initializeConnection;
