const express = require('express');
const { printErrorLog, formatErrorString, isEmpty, isNotEmpty, generatePassword, sendMail } = require("../utils/utils");
const router = express.Router();
const audit = require('../middleware/audit');
const { ErrorHandler } = require('../utils/error-handler');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models/User');


//Authenticate user
router.post('/login', audit, async (req, res, next) => {
    try {
        let rq = res.locals.json_req;     //Getting req_body from audit middleware after sanitizing them
        const { username, password } = rq;

        if (isEmpty(username) || isEmpty(password))
            return next(new ErrorHandler(300));

        //Validate if user exist in our database
        const user = await User.findOne({ username });

        //Matching Given password with stored user's password
        if (isNotEmpty(user) && (await bcrypt.compare(password, user.password))) {
            //Creating signed JWT
            const token = jwt.sign({ username: user.username }, process.env.JWT_SECRET, { expiresIn: "12h" });
            return res.status(200).json({
                result: "success",
                code: 200,
                desc: "User Logged In Successfully",
                token: token
            });
        } else {
            return next(new ErrorHandler("", "Error: Given username or password is invalid"));
        }
    }
    catch (error) {
        printErrorLog("user/login catch: " + formatErrorString(error));
        return next(error);
    }
});


//Register new user & storing their info in mongodb
router.post('/register', audit, async (req, res, next) => {
    try {
        const rq = res.locals.json_req;     //Getting req_body from audit middleware after sanitizing them
        const { name, username, email, age, income, policy_type } = rq;

        if (isEmpty(name) || isEmpty(username) || isEmpty(email) || isEmpty(policy_type))
            return next(new ErrorHandler(300));

        //Check if user already exist
        const old_user = await User.findOne({ username });

        if (isNotEmpty(old_user)) {
            return next(new ErrorHandler("", "User Already Exist. Please Login"));
        }

        //Generating random password and encrpting it with bcrypt
        const password = generatePassword(8);
        const encrypted_password = await bcrypt.hash(password, 10);

        //Storind data in db via mongoose User model
        await User.create({
            name,
            username,
            email,
            password: encrypted_password,
            age,
            income,
            policy_type
        });

        //Create JSON Web Token
        const token = jwt.sign({ username: username }, process.env.JWT_SECRET, { expiresIn: "12h" });

        //Sending Welcome Mail to registered user along with its generated password
        sendMail(email, "Welcome to Sun Insurance", `Dear ${name},\nWe warmly welcome to you on our platform.\nUse this password to login to your account: ${password}\nRegards,\nSun Insurance`);

        return res.status(200).json({
            result: "success",
            code: 200,
            desc: "User Registered Successfully",
            token: token
        });
    }
    catch (error) {
        printErrorLog("user/register catch: " + formatErrorString(error));
        return next(error);
    }
});



module.exports = router;
