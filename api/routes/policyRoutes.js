const express = require('express');
const { printErrorLog, formatErrorString, isEmpty } = require("../utils/utils");
const router = express.Router();
const audit = require('../middleware/audit');
const check_auth = require('../middleware/check-auth');
const { ErrorHandler } = require('../utils/error-handler');
const PolicyCategory = require('../models/PolicyCategory');
const Policy = require('../models/Policy');
const User = require('../models/User');


//Getting Policy Categories to populate in dropdown in Register Page UI
router.get('/getCategories', audit, check_auth, async (req, res, next) => {
    try {
        const policy_categories = await PolicyCategory.find({}).select('id name').exec();

        return res.status(200).json({
            result: "success",
            code: 200,
            desc: "Policy Categories Fetched Successfully",
            categories: policy_categories || []
        });
    }
    catch (error) {
        printErrorLog("policy/getCategories catch: " + formatErrorString(error));
        return next(error);
    }
});


//Get Policies which match criteria according to logged-in user policy type & policy type
router.get('/getRelatedPolicies', audit, check_auth, async (req, res, next) => {
    try {
        const username = req.user?.username;    //Getting logged-user username which was stored at check-auth middleware
        const user = await User.findOne({ username });

        if (isEmpty(user)) {
            return next(new ErrorHandler(401));
        }

        //Policies which match user criteria
        const policies = await Policy.find({
            min_income: { $lte: user.income },
            type: user.policy_type
        });

        return res.status(200).json({
            result: "success",
            code: 200,
            desc: "Policies Fetched Successfully",
            policies: policies || []
        });
    }
    catch (error) {
        printErrorLog("policy/getRelatedPolicies catch: " + formatErrorString(error));
        return next(error);
    }
});


module.exports = router;
