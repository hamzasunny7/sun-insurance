const winston = require('winston');
const expressWinston = require('express-winston');
const path = require('path');
const fs = require('fs');
const DailyRotateFile = require('winston-daily-rotate-file');

const isProd = process.env.NODE_ENV === 'production';

const logsDirectory = path.join(__dirname + "/../..", 'logs');
const expressLogsDirectory = path.join(logsDirectory, 'express');
const expressErrorsLogsDirectory = path.join(logsDirectory, 'express-errors');

// ensure log directory exists
fs.existsSync(expressLogsDirectory) || fs.mkdirSync(expressLogsDirectory, {recursive: true});
fs.existsSync(expressErrorsLogsDirectory) || fs.mkdirSync(expressErrorsLogsDirectory, {recursive: true});

const tsFormat = () => new Date().toLocaleTimeString();

const expressLogger = expressWinston.logger({
    format: winston.format.combine(
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.json()
    ),
    transports: [
        new winston.transports.Console(),
        new DailyRotateFile({
            filename: `${expressLogsDirectory}/express-logs.log`,
            timestamp: tsFormat,
            datePattern: 'YYYY-MM-DD',
            prepend: true,
            level: isProd ? 'info' : 'verbose'
        })
    ],
    meta: true,//Show Additional Hit Info
    // msg: "HTTP {{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
    expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
    exitOnError: false,//If false, handled exceptions will not cause process.exit
    ignoreRoute: function (req, res) {
        return [`/${process.env.ROUTE}/parseLocationData`].includes(req.url);//true will ignore,false will let show log
    } // optional: allows to skip some log messages based on request and/or response
});

const expressErrorLogger = expressWinston.errorLogger({
    format: winston.format.combine(
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.json()
    ),
    transports: [
        new DailyRotateFile({
            filename: `${expressErrorsLogsDirectory}/error-logs.log`,
            timestamp: tsFormat,
            handleExceptions: true,
            datePattern: 'YYYY-MM-DD',
            prepend: true,
            level: isProd ? 'info' : 'verbose'
        })
    ],
    exitOnError: false//If false, handled exceptions will not cause process.exit
});

module.exports = {
    expressLogger,
    expressErrorLogger
};
