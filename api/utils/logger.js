const bunyan = require('bunyan');
const path = require('path');
const fs = require('fs');
const {formatErrorString} = require("./utils");
const rfs = require('rotating-file-stream');
const cluster = require('cluster');
const _ = require('lodash');

const port = process.env.PORT || 3000;

// Prepare stream writable for log
const logsDirectory = path.join(__dirname + "/../..", 'logs');

// ensure log directory exists
fs.existsSync(logsDirectory) || fs.mkdirSync(logsDirectory, {recursive: true});

const bunyanApiLogName = "insurance_api";
const bunyanCronApiLogName = "cron_api";

const pad = num => {
    return (num > 9 ? "" : "0") + num;
};

const nameGenerator = (time, index, fileName) => {
    if (!time) return `${fileName}.log`;
    const year = time.getFullYear();
    const month = pad(time.getMonth() + 1);
    const day = pad(time.getDate());
    // const hour = pad(time.getHours());
    // const minute = pad(time.getMinutes());
    // return `${fileName}_${year}-${month}-${day}-${hour}${minute}.log`;
    return `${fileName}_${year}-${month}-${day}.log`;
};

const apiLogNameGenerator = (time, index) => {
    let fileName = "apis-access-" + port;
    return nameGenerator(time, index, fileName);
};

const cronNameGenerator = (time, index) => {
    let fileName = "cron-apis-access-" + port;
    return nameGenerator(time, index, fileName);
};

// Collect logger messages from child processes and output them to the
// master's logger instance. This avoids any wackiness with multiple
// processes trying to write to the same stream (particularly if the
// `rotating-file` stream type is used)
//
// **See**: https://github.com/trentm/node-bunyan/issues/117
if (cluster.isMaster) {
    const apiLogStream = rfs.createStream(apiLogNameGenerator, {
        interval: '1d', // rotates at every midnight
        // initialRotation: true,
        path: logsDirectory
    });
    // here are reported non blocking errors
    apiLogStream.on('warning', (err) => {
        console.log("apiLogStream Warning: " + formatErrorString(err));
    });
    apiLogStream.on('rotated', (filename) => {
        console.log(`apiLogStream '${filename}' rotated successfully`);
    });

    const cronLogStream = rfs.createStream(cronNameGenerator, {
        interval: '1d', // rotates at every midnight
        // initialRotation: true,
        path: logsDirectory
    });
    // here are reported non blocking errors
    cronLogStream.on('warning', (err) => {
        console.log("cronLogStream Warning: " + formatErrorString(err));
    });
    cronLogStream.on('rotated', (filename) => {
        console.log(`apiLogStream '${filename}' rotated successfully`);
    });

    const bunyanApiLog = bunyan.createLogger({
        name: bunyanApiLogName,
        streams: [{
            stream: apiLogStream
        }]
    });
    // bunyanApiLog.fields = {time_stamp: 0, request_id: 1, api_name: 2, type: 3, api_detail: 4, audit_p_detail: 5, ip: 6}; //For custom Column order but has problem

    const bunyanCronApiLog = bunyan.createLogger({
        name: bunyanCronApiLogName,
        streams: [{
            stream: cronLogStream
        }]
    });
    // bunyanCronApiLog.fields = {time_stamp: 0, request_id: 1, api_name: 2, type: 3, cron_name: 4, api_detail: 5}; //For custom Column order but has problem

    module.exports = {
        bunyanApiLog,
        bunyanCronApiLog,
        bunyanApiLogName,
        bunyanCronApiLogName
    };
}
else {
    module.exports = {
        bunyanApiLogPrint: function () {
            process.send({type: 'api_info', level: 'info', message: Array.prototype.slice.call(arguments)});
        },
        bunyanCronApiLogPrint: function () {
            process.send({type: 'cron_info', level: 'info', message: Array.prototype.slice.call(arguments)});
        },
        bunyanApiLogName,
        bunyanCronApiLogName
    };
}
