const express = require('express');
const cors = require('cors');
const app = express();
require("./api/config/mongoose").connect();
const nocache = require('nocache');
const bodyParser = require('body-parser');
const error_handler = require('./api/utils/error-handler');
const {isJsonStr} = require('./api/utils/utils');
const {createUserApiLog} = require('./api/models/LogModel');
const userRoutes = require('./api/routes/userRoutes');
const policyRoutes = require('./api/routes/policyRoutes');
const requestIp = require('request-ip');
const {expressLogger, expressErrorLogger} = require('./api/utils/winston-logger');
const endMw = require('express-end');

//----------------------------Middleware to turn off caching
//If We don't disable cache api will not get data from db id params same need to turn off cache
app.use(nocache());
//----------------------------------Middleware Ended--------

//Order of this route matters need to place this above store log middleware as it's returning empty result and we don't need to store record of this
app.get('/' + process.env.ROUTE + '/pingServer', (req, res) => {    //Route to Ping & check if Server is online
    res.status(200).send("");//Return only empty string as on android side a check is placed that result should be of 0 content
});

//----------------------------Middleware for accepting encoded & json request parms
app.use(express.urlencoded({extended: true}));
app.use(express.json());
//----------------------------------Middleware Ended-------------------------------

//----------------------------Middleware for capturing request is actually ended even though listener is timed out
app.use(endMw);
//----------------------------------Middleware Ended-------------------------------

//----------------------------Middleware for reading raw Body as text use req.body
app.use(bodyParser.text({type: 'text/plain', limit: '50mb'}));
//----------------------------------Middleware Ended-------------------------------

//----------------------------Middleware for Getting a user's IP
app.use(requestIp.mw());
//----------------------------------Middleware Ended-------------------------------

//----------------------------Middleware for printing extra logs & and saving logs in file
app.use(expressLogger);
//----------------------------------Middleware Ended-------------------------------------

//----------------------------Middleware to Fix CORS Errors This Will Update The Incoming Request before sending to routes
app.use(cors())
//--------------------------------------------------------Middleware Ended----------------------------------------------

//-----------------------------Middleware for storing API logs into DB
app.use(function (req, res, next) {
    // Do whatever you want this will execute when response is finished
    res.once('end', function () {
        createUserApiLog(req, res);
    });

    // Save Response body
    let oldSend = res.send;
    res.send = function (data) {
        res.locals.res_body = isJsonStr(data) ? JSON.parse(data) : data;
        oldSend.apply(res, arguments);
    };
    next();
});
//--------------------------------------------------------Middleware Ended----------------------------------------------


// Routes which should handle requests
app.use('/' + process.env.ROUTE + '/user', userRoutes);
app.use('/' + process.env.ROUTE + '/policy', policyRoutes);


//----------------------------Middleware for catching 404 and forward to error handler
app.use((req, res, next) => {
    const error = new Error(error_handler.ERROR_404);
    error.statusCode = 404;
    next(error);
});

//Error handler
app.use((error, req, res, next) => {
    // Because we hooking post-response processing into the global error handler, we
    // get to leverage unified logging and error handling; but, it means the response
    // may have already been committed, since we don't know if the error was thrown
    // PRE or POST response. As such, we have to check to see if the response has
    // been committed before we attempt to send anything to the user.
    if (!res.headersSent) {
        res.status(error.statusCode || 500);
        res.json({
            result: "error",
            code: error.statusCode || 500,
            desc: error.message
        });
    }
});

//Best Tested place that store only uncaught errors
app.use(expressErrorLogger);


module.exports = app;
